# devops-intern-recruitment

The task to complete during the recruitment process: 

Create a Docker image, which when run as a container, runs a bundled shell script that analyzes the contents of a given directory.    

The directory to analyze should be set by an environment variable, `DIR_TO_ANALYZE`. It will be passed to the container as a Docker volume.    

The script should output the following:    

- Full paths of the 3 biggest regular files and their human-readable sizes    
- The number of regular files     
- The total size of the directory    

Example:
```
% docker run -it -e DIR_TO_ANALYZE=/data -v /GIT_REPOS/some-repo:/data homework
Analyzing /data...
...ok
      
3 biggest files:
/data/sandbox_automation/.terraform/plugins/linux_amd64/terraform-provider-aws_v1.56.0_x4: 101M
/data/other_automation/.terraform/plugins/linux_amd64/terraform-provider-aws_v1.56.0_x4: 101M
/data/sandbox_automation/.terraform/plugins/linux_amd64/terraform-provider-template_v2.0.0_x4: 20M

Total number of regular files: 2771

Total directory size: 279M
```

Please note that this test is not about solving a particular problem. It's about the execution, style & checking what's your default way of thinking.
